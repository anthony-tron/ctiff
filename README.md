# Get started

Documentation is available at <https://anthony-tron.gitlab.io/ctiff/annotated.html>.

## Requirements

1. cmake
2. libtiff-dev

## Installation

Move to the directory you've cloned and run:
``` sh
chmod u+x install.sh
./install.sh
```
It will fail if you already have a `build` directory. If you need to rerun ./install.sh, run `rm -r build`.

*ctiff* should now be installed in your operating system's preferred location as a **static library**. You can check the paths thanks to `make`'s output.

For instance, on *Ubuntu 20.04*, it is located in :

- `/usr/local/lib/libctiff.a`
- `/usr/local/include/ctiff/`

## Usage

### qmake

Add this to your `.pro` file:
```qmake
LIBS += -lctiff -ltiff
```

### cmake

Create a new project and add `CMakeLists.txt` with this content :

```cmake
cmake_minimum_required(VERSION 3.5)

project(my_project LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_library(ctiff REQUIRED) # ctiff
find_library(tiff REQUIRED) # libtiff

add_executable(my_project main.cpp)
target_link_libraries(my_project LINK_PUBLIC ctiff tiff)
```

Then, type :

```sh
mkdir build
cd build
cmake ..
make
```

Your program should now be ready to run: `./my_project`

You should be able to include *ctiff* headers with
```cpp
#include <ctiff/Tiff.h>
#include <ctiff/PixelBuffer.h>
#include <ctiff/utils.h>
```


