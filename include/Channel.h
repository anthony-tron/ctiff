#ifndef CHANNEL_H
#define CHANNEL_H

namespace ctiff
{
    /**
     * @brief The Channel enum
     */
    enum Channel
    {
        unique = 0,     ///< for 1-channel image

        red = 0,        ///< for RGB.A images
        green = 1,      ///< for RGB.A images
        blue = 2,       ///< for RGB.A images
        alpha = 3,      ///< for RGBA images

        cyan = 0,       ///< for CMYK images
        magenta = 1,    ///< for CMYK images
        yellow = 2,     ///< for CMYK images
        black = 3,      ///< for CMYK images
    };
}

#endif // CHANNEL_H
