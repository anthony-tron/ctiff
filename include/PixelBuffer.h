#ifndef PIXELBUFFER_H
#define PIXELBUFFER_H

#include <cstddef>
#include <cstdint>
#include "Channel.h"
#include "PixelBufferPipeline.h"

namespace ctiff
{
    class PixelBuffer
    {
        public:
        /**
         * @brief PixelBuffer allocator constructor (1).
         * Allocate memory and create a new buffer with length of (\p pixelCount * \p bytesPerPixel) bytes.
         * @note The buffer is automatically freed by the destructor.
         * @param pixelCount number of pixel
         * @param bytesPerPixel number of bytes per pixel (generally same as samples per pixel)
         */
        PixelBuffer(size_t pixelCount, uint8_t bytesPerPixel);

        PixelBuffer(PixelBuffer&&);

        PixelBuffer(const PixelBuffer&) = delete;
        PixelBuffer& operator=(const PixelBuffer&) = delete;

        /**
         * @brief PixelBuffer pointer constructor (2).
         * Create a high interface to manipulate buffer.
         * @warning the destructor will not call free() for you. **Call it only if needed**.
         * @param buffer pointer to the buffer to process
         * @param length length of the buffer
         * @param bytesPerPixel number of bytes per pixel (generally same as samples per pixel)
         */
        PixelBuffer(uint8_t* buffer, size_t length, uint8_t bytesPerPixel);

        /**
         * @brief PixelBuffer destructor
         * @note free() is called at this point for objects created with constructor (1), but not with constructor (2)
         */
        ~PixelBuffer();

        /**
         * @brief operator uint8_t*() const
         * Convert this object into uint8_t*.
         * @example uint8_t* buffer(pixelBuffer);
         */
        operator uint8_t*() const;

        /**
         * @brief internalBuffer
         * @see operator uint8_t*()
         * @return the internal buffer
         */
        uint8_t* internalBuffer() const;

        /**
         * @brief free
         * free the buffer using `delete[]`
         */
        void free();

        /**
         * @brief readAt
         * @param pixelOffset pixel index
         * @param byteOffset should be 0, 1, 2, 3 or one of `Channel` constants
         * @note use static_cast<Channel>(int) if needed
         * @return data at pixel n° pixelOffset at n° byteOffset channel.
         */
        uint8_t readAt(uint32_t, Channel);

        /**
         * @brief writeAt
         * @param pixelOffset pixel index
         * @param byteOffset should be 0, 1, 2, 3 or one of `Channel` constants
         * @param data to write at this location
         * @note use `static_cast<Channel>(int)` if needed
         */
        void writeAt(uint32_t, Channel, uint8_t);

        /**
         * @brief slice
         * Copy and paste a series of data into an new buffer
         * @note consider using `begin() + offset` when possible
         * @param pixelStart index of the first pixel to capture
         * @param pixelCount > 0
         * @return new PixelBuffer with copied data
         */
        PixelBuffer slice(size_t pixelStart, size_t pixelCount);

        /**
         * @brief at
         * @param offset pixel index
         * @return PixelBufferPipeline
         * @see PixelBufferPipeline
         */
        PixelBufferPipeline at(uint32_t);

        /**
         * @todo test
         * @brief extract a channel into a new buffer
         * @return new PixelBuffer
         */
        PixelBuffer extract(Channel);

        uint8_t* begin();
        uint8_t* end();

        uint8_t* rbegin();
        uint8_t* rend();

        // getters
        size_t length() const;
        uint8_t bytesPerPixel() const;


        private:
        size_t m_length;
        uint8_t* m_buffer;
        uint8_t m_bytesPerPixel;
        bool m_shouldFreeOnDestruct;
    };
}

#endif // PIXELBUFFER_H
