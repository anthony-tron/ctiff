#ifndef PIXELBUFFERPIPELINE_H
#define PIXELBUFFERPIPELINE_H

#include <cstdint>
#include "Channel.h"

namespace ctiff
{
    class PixelBuffer;

    class PixelBufferPipeline
    {
        public:
        /**
         * @brief PixelBufferPipeline constructor (1)
         * @param buffer reference to a PixelBuffer object
         * @param offset pixel index to process
         * @see PixelBuffer
         */
        PixelBufferPipeline(PixelBuffer&, uint32_t offset);

        /**
         * @brief write
         * @param channel the channel that contains data to edit
         * @param data new data
         * @return reference to self
         * @see Channel
         */
        PixelBufferPipeline& write(Channel, uint8_t);

        private:
        PixelBuffer& m_buffer;
        uint32_t m_offset;
    };
}

#endif // PIXELBUFFERPIPELINE_H
