#ifndef TIFF_H
#define TIFF_H

extern "C" {
    #include <tiffio.h>
}

#include <string>
#include "PixelBuffer.h"

namespace ctiff
{
    /**
     * @brief The class Tiff:
     * a high level class to deal with tiff formatted images.
     */
    class Tiff
    {
        public:
        /**
         * @brief AccessMode
         * does not support reading and writing at the same time.
         */
        enum class AccessMode { read, write, };

        /**
         * @brief constructor Tiff
         * @param filename the path to the tiff file
         * @param accessMode read or write @see AccessMode
         * @throws std::runtime_error when
         *      <br/> Access is denied
         *      <br/> File does not exist
         *      <br/> Unknown error
         */
        Tiff(const std::string&, AccessMode);

        Tiff(Tiff&&);

        Tiff(const Tiff&) = delete;
        Tiff& operator=(const Tiff&) = delete;

        ~Tiff();

        void close() const;

        /**
         * @brief reading a field of type T
         * @param tag of the field to get
         * @return the value of the field to get
         * @throws TODO std::exception when
         *      <br/> field is not set
         *      <br/> tag does not exist
         *      <br/> T does not match the appropriate type
         */
        template <class T>
        T field(uint32) const;

        /**
         * @brief writing a field of type T
         * @param tag of the field to write
         * @param value of the field to write
         * @throws std::exception when
         *      <br/> field is not set
         *      <br/> tag does not exist
         *      <br/> T does not match the appropriate type
         * @return reference to self
         */
        template <class T>
        Tiff& withField(uint32, T value);

        /**
         * @brief sets width and height for the image.
         * @param width > 0
         * @param height > 0
         * @throws std::range_error if width and / or height is null
         * @return reference to self
         */
        Tiff& withDimensions(uint32, uint32);

        /**
         * @brief sets appropriate tags for working with CMYK colorspace:
         *      <br/> BitsPerSample=8
         *      <br/> SamplesPerPixel=4
         *      <br/> Photometric=Separated
         * @return reference to self
         */
        Tiff& withCmyk();

        /**
         * @brief withBw
         * sets appropriate types for working with greyscale colorspace:
         * 	<br/> BitsPerSample=8
         * 	<br/> SamplesPerPixel=1
         * 	<br/> Photometric=MinIsWhite|MinIsBlack
         * @param minIsWhite : true = MinIsWhite, false = MinIsBlack
         * @return
         */
        Tiff& withBw(bool minIsWhite = true);

        /**
         * @brief sets planar config to Contiguous.
         * @return reference to self
         */
        Tiff& withContiguousFormat();

        /**
         * @brief sets color space aka photometric field
         * @param colorSpace one of <br/> PHOTOMETRIC_MINISWHITE
         *                          <br/> PHOTOMETRIC_MINISBLACK
         *                          <br/> PHOTOMETRIC_RGB
         *                          <br/> PHOTOMETRIC_PALETTE
         *                          <br/> PHOTOMETRIC_MASK
         *                          <br/> PHOTOMETRIC_SEPARATED
         *                          <br/> PHOTOMETRIC_YCBCR
         *                          <br/> PHOTOMETRIC_CIELAB
         *                          <br/> PHOTOMETRIC_ICCLAB
         *                          <br/> PHOTOMETRIC_ITULAB
         *                          <br/> PHOTOMETRIC_CFA
         *                          <br/> PHOTOMETRIC_LOGL
         * @return reference to self
         */
        Tiff& withColorSpace(uint16);

        /**
         * @brief sets bits per sample.
         * Bits per sample are the number of bits per channel.
         * @param bitsPerSample should be 8, 16, 24 or 32
         * @throws std::out_of_range when
         *      <br/> bitsPerSample is null
         * @return reference to self
         */
        Tiff& withBitsPerSample(uint16);

        /**
         * @brief sets samples per pixel.
         * Samples per pixel are the number of channel per pixel.
         * @param samplesPerPixel in [1; 4]
         * @throws std::range_error if samplesPerPixel is out of [1; 4]
         * @return reference to self
         */
        Tiff& withSamplesPerPixel(uint16);

        /**
         * @brief returns pointer to internal TIFF struct
         * @return TIFF*
         */
        TIFF* internalTiff() const;

        /**
         * @brief accessMode of opened TIFF
         * @return AccessMode
         */
        AccessMode accessMode() const;

        /**
         * @brief get width
         * @return width of the image
         */
        uint32 width() const;

        /**
         * @brief get height
         * @return height of the image
         */
        uint32 height() const;

        /**
         * @brief colorSpace get colorSpace aka photometric interpretation
         * @return
         */
        uint16 colorSpace() const;

        /**
         * @brief bitsPerSample
         * most of the time, this will return 8
         * @return
         */
        uint16 bitsPerSample() const;

        /**
         * @brief samplesPerPixel
         * will return values like 1, 3, and 4
         * @return
         */
        uint16 samplesPerPixel() const;

        /**
         * @brief convert to raw RGBA buffer
         * Automatically converts source data to RGBA data.
         * @return PixelBuffer containing raw data
         * @see PixelBuffer
         */
        PixelBuffer toRGBA() const;

        /**
         * @brief scan a line in the image
         * @bug does not work on tiled images
         * @param row the index of the row to read
         * @param sample the index of the layer to read
         * @return PixelBuffer
         * @see PixelBuffer
         */
        PixelBuffer scanLine(uint32 row, uint16 sample = 0) const;

        /**
         * @brief scans all lines
         * @bug does not work on tiled images
         * @param sample the index of the layer to read
         * @return PixelBuffer
         * @see PixelBuffer
         */
        PixelBuffer scanEveryLine(uint16 sample = 0) const;

        /**
         * @brief write a line to the image
         * @param buffer raw data to write. PixelBuffer can be converted to uint8*.
         * @param row index of the row to write
         */
        void writeLine(uint8*, uint32 row);


        private:
        AccessMode m_accessMode;
        TIFF* m_tiff;
    };

    template <class T>
    T Tiff::field(uint32 tag) const
    {
        T value;
        TIFFGetField(m_tiff, tag, &value);
        return value;
    }

    template <class T>
    Tiff& Tiff::withField(uint32 tag, T value)
    {

        if (TIFFSetField(m_tiff, tag, value) == 0)
            throw new std::exception();

        return *this;
    }
}

#endif // TIFF_H
