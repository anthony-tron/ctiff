#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include <array>
#include "Tiff.h"

namespace ctiff
{
    typedef std::array<uint32_t, 256> histogram_t;
    histogram_t computeHistogram(const Tiff&, Channel);
    std::vector<histogram_t> computeAllHistograms(const Tiff&);
}

#endif // UTILS_H
