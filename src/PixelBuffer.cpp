#include <cstring>
#include <stdexcept>
#include "PixelBuffer.h"
#include "PixelBufferPipeline.h"

namespace ctiff
{
    PixelBuffer::PixelBuffer(size_t pixelCount, uint8_t bytesPerPixel)
        : m_length(pixelCount * bytesPerPixel),
          m_buffer(new uint8_t[m_length]),
          m_bytesPerPixel(bytesPerPixel),
          m_shouldFreeOnDestruct(true)
    {}

    PixelBuffer::PixelBuffer(PixelBuffer&& other)
        : m_length(other.m_length),
          m_buffer(other.m_buffer),
          m_bytesPerPixel(other.m_bytesPerPixel),
          m_shouldFreeOnDestruct(true)
    {
        other.m_buffer = nullptr;
    }

    PixelBuffer::PixelBuffer(uint8_t* buffer, size_t length, uint8_t bytesPerPixel)
        : m_length(length),
          m_buffer(buffer),
          m_bytesPerPixel(bytesPerPixel),
          m_shouldFreeOnDestruct(false)
    {}

    PixelBuffer::~PixelBuffer()
    {
        if (m_shouldFreeOnDestruct)
            free();
    }

    PixelBuffer::operator uint8_t *() const
    {
        return m_buffer;
    }

    void PixelBuffer::free()
    {
        delete[] m_buffer;
    }

    uint8_t* PixelBuffer::internalBuffer() const
    {
        return m_buffer;
    }

    uint8_t PixelBuffer::readAt(uint32_t pixelOffset, Channel byteOffset)
    {
        return m_buffer[(pixelOffset * m_bytesPerPixel) + byteOffset];
    }

    void PixelBuffer::writeAt(uint32_t pixelOffset, Channel byteOffset, uint8_t data)
    {
        m_buffer[(pixelOffset * m_bytesPerPixel) + byteOffset] = data;
    }

    PixelBuffer PixelBuffer::slice(size_t pixelStart, size_t pixelCount)
    {
        if (pixelCount == 0)
            throw std::range_error("Cannot slice 0 pixels");

        const size_t length = pixelCount * m_bytesPerPixel;
        const size_t startOffset = pixelStart * m_bytesPerPixel;

        uint8_t* copy = new uint8_t[length];
        std::memcpy(copy, m_buffer + startOffset, length);

        return PixelBuffer(copy, length, m_bytesPerPixel);
    }

    PixelBufferPipeline PixelBuffer::at(uint32_t offset)
    {
        return PixelBufferPipeline(*this, offset);
    }

    PixelBuffer PixelBuffer::extract(Channel channel)
    {
        // same number of pixels, but 1 byte (8 bits) per channel
        size_t pixelCount = m_length / m_bytesPerPixel;
        PixelBuffer layer(pixelCount, 1);

        size_t i = 0;
        for (uint8_t* ptr = begin() + channel;
             i < pixelCount && ptr < end();
             ptr += bytesPerPixel(), ++i)
            layer.writeAt(i, Channel::unique, *ptr);

        return layer;
    }

    uint8_t* PixelBuffer::begin()
    {
        return m_buffer;
    }

    uint8_t* PixelBuffer::end()
    {
        return m_buffer + m_length;
    }

    uint8_t* PixelBuffer::rbegin()
    {
        return end() - bytesPerPixel();
    }

    uint8_t *PixelBuffer::rend()
    {
        return begin() - 1;
    }

    size_t PixelBuffer::length() const
    {
        return m_length;
    }

    uint8_t PixelBuffer::bytesPerPixel() const
    {
        return m_bytesPerPixel;
    }
}
