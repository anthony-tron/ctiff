#include "PixelBufferPipeline.h"
#include "PixelBuffer.h"

namespace ctiff
{
    PixelBufferPipeline::PixelBufferPipeline(PixelBuffer& buffer, uint32_t offset)
        : m_buffer(buffer), m_offset(offset)
    {}

    PixelBufferPipeline& PixelBufferPipeline::write(Channel channel, uint8_t data)
    {
        m_buffer.writeAt(m_offset, channel, data);
        return *this;
    }
}
