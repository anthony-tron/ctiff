#include "Tiff.h"
#include <stdexcept>
#include <iostream>

namespace ctiff
{
    Tiff::Tiff(const std::string& filename, AccessMode accessMode)
        : m_accessMode(accessMode),
          m_tiff(TIFFOpen(filename.c_str(), accessMode == AccessMode::read ? "r" : "w"))
    {
        if (m_tiff == nullptr)
            throw std::runtime_error(filename + " cannot be opened");
    }

    Tiff::Tiff(Tiff&& other)
        : m_accessMode(other.m_accessMode),
          m_tiff(other.m_tiff)
    {
        other.m_tiff = nullptr;
    }

    Tiff::~Tiff()
    {
        close();
    }

    void Tiff::close() const
    {
        if (m_tiff != nullptr)
            TIFFClose(m_tiff); // close
    }

    Tiff& Tiff::withDimensions(uint32 width, uint32 height)
    {
        if (width == 0 || height == 0)
            throw std::range_error("Width, height may not be 0");

        return (*this)
            .withField(TIFFTAG_IMAGEWIDTH, width)
            .withField(TIFFTAG_IMAGELENGTH, height);
    }

    Tiff &Tiff::withBw(bool minIsWhite)
    {
        return (*this)
            .withColorSpace(minIsWhite ? PHOTOMETRIC_MINISWHITE : PHOTOMETRIC_MINISBLACK)
            .withBitsPerSample(8)
            .withSamplesPerPixel(1);
    }

    Tiff &Tiff::withCmyk()
    {
        return (*this)
            .withField<uint16>(TIFFTAG_BITSPERSAMPLE, 8) // 1 byte per channel
            .withField<uint16>(TIFFTAG_SAMPLESPERPIXEL, 4) // 4 channels
            .withField<uint16>(TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_SEPARATED);
    }

    Tiff &Tiff::withContiguousFormat()
    {
        return (*this)
            .withField<uint16>(TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
    }

    Tiff &Tiff::withColorSpace(uint16 colorSpace)
    {
        if (colorSpace > 10
            && colorSpace != PHOTOMETRIC_LOGL
            && colorSpace != PHOTOMETRIC_LOGLUV)
            throw std::domain_error("Color space aka Photometric's field is out of domain");

        return (*this)
            .withField<uint16>(TIFFTAG_PHOTOMETRIC, colorSpace);
    }

    Tiff &Tiff::withBitsPerSample(uint16 bitsPerSample)
    {
        if (bitsPerSample == 0)
            throw std::out_of_range("Bits per sample of 0 is illegal");

        if (bitsPerSample != 8
            && bitsPerSample != 16
            && bitsPerSample != 24
            && bitsPerSample != 32)
            std::cerr
                << "Warning: bitsPerSample of "
                << bitsPerSample
                << " is not guaranteed to be properly supported."
                << std::endl;

        return (*this)
            .withField<uint16>(TIFFTAG_BITSPERSAMPLE, bitsPerSample);
    }

    Tiff& Tiff::withSamplesPerPixel(uint16 samplesPerPixel)
    {
        if (samplesPerPixel == 0 || samplesPerPixel > 4)
            throw std::range_error("Samples per pixel must be in [1; 4]");

        return (*this)
                .withField<uint16>(TIFFTAG_SAMPLESPERPIXEL, samplesPerPixel);
    }


    TIFF* Tiff::internalTiff() const
    {
        return m_tiff;
    }

    Tiff::AccessMode Tiff::accessMode() const
    {
        return m_accessMode;
    }

    uint32 Tiff::width() const
    {
        return field<uint32>(TIFFTAG_IMAGEWIDTH);
    }

    uint32 Tiff::height() const
    {
        return field<uint32>(TIFFTAG_IMAGELENGTH);
    }

    uint16 Tiff::colorSpace() const
    {
        return field<uint16>(TIFFTAG_PHOTOMETRIC);
    }

    uint16 Tiff::bitsPerSample() const
    {
        return field<uint16>(TIFFTAG_BITSPERSAMPLE);
    }

    uint16 Tiff::samplesPerPixel() const
    {
        return field<uint16>(TIFFTAG_SAMPLESPERPIXEL);
    }

    PixelBuffer Tiff::toRGBA() const
    {
        uint32 width = this->width();
        uint32 height = this->height();

        tmsize_t bufferSize = width * height * sizeof(uint32);
        uint32* raster = reinterpret_cast<uint32*>(_TIFFmalloc(bufferSize));

        if (raster == nullptr)
            throw std::exception();

        if (TIFFReadRGBAImage(m_tiff, width, height, raster) == 0)
            throw std::exception();

        uint8* buffer = reinterpret_cast<uint8*>(raster);
        return PixelBuffer(buffer, bufferSize, 4);
    }

    PixelBuffer Tiff::scanLine(uint32 row, uint16 sample) const
    {
        const uint16 _samplesPerPixel = field<uint16>(TIFFTAG_SAMPLESPERPIXEL);
        PixelBuffer buffer(TIFFScanlineSize(m_tiff) / _samplesPerPixel, _samplesPerPixel);

        if (TIFFReadScanline(m_tiff, buffer, row, sample) == -1)
            throw std::exception();

        return buffer;
    }

    PixelBuffer Tiff::scanEveryLine(uint16 sample) const
    {
        const uint32 _width(width());
        const uint32 _height(height());
        const uint16 _samplesPerPixel = field<uint16>(TIFFTAG_SAMPLESPERPIXEL);
        const uint32 offset = _samplesPerPixel * _width; // memory offset to jump from one line to another

        PixelBuffer buffer((TIFFScanlineSize(m_tiff) * _height) / _samplesPerPixel, _samplesPerPixel);
        uint8* ptr = buffer; // memory address, where to copy the scanned line

        for (uint32 i = 0; i < _height; ++i)
        {
            if (TIFFReadScanline(m_tiff, ptr, i, sample) == -1)
                throw std::exception();

            ptr += offset; // point to next line
        }

        return buffer;
    }

    void Tiff::writeLine(uint8* buffer, uint32 row)
    {
        if (TIFFWriteScanline(m_tiff, buffer, row) == -1)
            throw std::exception();
    }
}
