#include "utils.h"


namespace ctiff
{
    histogram_t computeHistogram(const Tiff& tiff, Channel channel)
    {
        histogram_t histogram{{0}};

        for (uint32 i = 0; i < tiff.height(); ++i)
        {
            PixelBuffer line(tiff.scanLine(i));
            for (uint32 j = 0; j < tiff.width(); ++j)
                histogram[line.readAt(j, channel)] += 1;
        }

        return histogram;
    }

    std::vector<histogram_t> computeAllHistograms(const Tiff& tiff)
    {
        std::vector<histogram_t> histograms;

        const uint16 channelCount = tiff.field<uint16>(TIFFTAG_SAMPLESPERPIXEL);
        if (channelCount == 0)
            throw std::runtime_error("cannot find any channels");

        histograms.resize(channelCount);

        for (uint32 i = 0; i < tiff.height(); ++i)
        {
            PixelBuffer line(tiff.scanLine(i));
            for (uint32 j = 0; j < tiff.width(); ++j)
                for (uint16 k = 0; k < channelCount; ++k)
                    histograms[k][line.readAt(j, static_cast<Channel>(k))] += 1;
        }

        return histograms;
    }
}
