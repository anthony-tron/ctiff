# Unit Testing

This project uses `googletest` to for its unit tests.

## Make a new test suite

Modify `CMakeLists.txt` to add a test suite named `test_custom` to `ALL_TESTS`:

```cmake
list(APPEND ALL_TESTS
    # ...
    test_custom # <- test_custom.cpp must exist
)
```

## Run a test suite

```bash
./build/test/test_custom
```

