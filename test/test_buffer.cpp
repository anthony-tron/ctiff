#include <gtest/gtest.h>
#include "PixelBuffer.h"

using ctiff::Channel;
using ctiff::PixelBuffer;

TEST(test_buffer, slice)
{
    PixelBuffer buffer(8, 4);
    for (size_t i = 0; i < 8; ++i)
        buffer.at(i)
            .write(Channel::cyan, 0xFF)
            .write(Channel::magenta, 0x00)
            .write(Channel::yellow, 0xBB)
            .write(Channel::black, 0x01);
    buffer.at(6)
        .write(Channel::cyan, 0xAA)
        .write(Channel::magenta, 0xBB)
        .write(Channel::yellow, 0xCC)
        .write(Channel::black, 0xDD);

    PixelBuffer first = buffer.slice(0, 1);
    ASSERT_EQ(first.length(), 4);
    EXPECT_EQ(first.readAt(0, Channel::cyan), 0xFF);
    EXPECT_EQ(first.readAt(0, Channel::magenta), 0x00);
    EXPECT_EQ(first.readAt(0, Channel::yellow), 0xBB);
    EXPECT_EQ(first.readAt(0, Channel::black), 0x01);

    PixelBuffer second = buffer.slice(6, 1);
    ASSERT_EQ(second.length(), 4);
    EXPECT_EQ(second.readAt(0, Channel::cyan), 0xAA);
    EXPECT_EQ(second.readAt(0, Channel::magenta), 0xBB);
    EXPECT_EQ(second.readAt(0, Channel::yellow), 0xCC);
    EXPECT_EQ(second.readAt(0, Channel::black), 0xDD);
}
