#include <gtest/gtest.h>
#include "Tiff.h"

#define SRC "/var/public_img/libtiff/pc260001.tif"

using ctiff::Tiff;

TEST(test_copy, copyLinePerLine)
{
    EXPECT_NO_THROW({
        Tiff src(SRC, Tiff::AccessMode::read);
        Tiff out("/var/public_img/test0.tif", Tiff::AccessMode::write);
        out.withContiguousFormat()
           .withDimensions(src.width(), src.height())
           .withColorSpace(src.field<uint16_t>(TIFFTAG_PHOTOMETRIC))
           .withBitsPerSample(src.field<uint16_t>(TIFFTAG_BITSPERSAMPLE))
           .withSamplesPerPixel(src.field<uint16_t>(TIFFTAG_SAMPLESPERPIXEL));

        for (size_t i = 0; i < out.height(); ++i)
        {
            out.writeLine(src.scanLine(i), i);
        }
    });
}

TEST(test_copy, copyAllAtOnce)
{
    EXPECT_NO_THROW({
        Tiff src(SRC, Tiff::AccessMode::read);
        Tiff out("/var/public_img/test1.tif", Tiff::AccessMode::write);
        out.withContiguousFormat()
           .withDimensions(src.width(), src.height())
           .withColorSpace(src.field<uint16_t>(TIFFTAG_PHOTOMETRIC))
           .withBitsPerSample(src.field<uint16_t>(TIFFTAG_BITSPERSAMPLE))
           .withSamplesPerPixel(src.field<uint16_t>(TIFFTAG_SAMPLESPERPIXEL));

        uint8_t* data(src.scanEveryLine());
        uint32_t offset(out.width() * out.field<uint16_t>(TIFFTAG_SAMPLESPERPIXEL));

        for (size_t i = 0; i < out.height(); ++i)
        {
            out.writeLine(data + i * offset, i);
        }
    });
}

