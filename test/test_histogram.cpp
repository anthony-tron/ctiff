#include <gtest/gtest.h>
#include "utils.h"

using ctiff::Channel;
using ctiff::Tiff;

TEST(test_histogram, oneChannelHistogram)
{
    Tiff tiff("/home/anthony/Pictures/tiff/simpleTest.tiff", Tiff::AccessMode::read);

    ASSERT_NO_THROW({
        ctiff::histogram_t histogram = computeHistogram(tiff, Channel::unique);

        for (uint16 i = 0; i != histogram.size(); ++i)
            std::cout << std::hex << +i << ": " << std::dec << +histogram[i] << std::endl;
    });
}

TEST(test_histogram, generic_histogram)
{
    Tiff tiff("/home/anthony/Pictures/tiff/simpleTest.tiff", Tiff::AccessMode::read);

    ASSERT_NO_THROW({

        std::vector<ctiff::histogram_t> histograms(computeAllHistograms(tiff));

        for (uint16 i = 0; i != histograms.size(); ++i)
        {
            std::cout << i << " ===" << std::endl;
            for (size_t j = 0; j < histograms[0].size(); ++j)
                std::cout << std::hex << j << ": " << +histograms[i][j] << std::endl;
            std::cout << "=====" << std::endl;
        }
    });
}
