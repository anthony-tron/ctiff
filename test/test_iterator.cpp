#include <gtest/gtest.h>
#include "PixelBuffer.h"

using ctiff::Channel;
using ctiff::PixelBuffer;

TEST(test_iterator, normal_iterators_with_solid_white_color)
{
    PixelBuffer buffer(16, 4);
    for (uint8_t* ptr(buffer.begin()); ptr != buffer.end(); ++ptr)
        *ptr = 0xDD;

    for (size_t i = 0; i < 16; ++i)
    {
        EXPECT_EQ(buffer.readAt(i, Channel::cyan), 0xDD);
        EXPECT_EQ(buffer.readAt(i, Channel::magenta), 0xDD);
        EXPECT_EQ(buffer.readAt(i, Channel::yellow), 0xDD);
        EXPECT_EQ(buffer.readAt(i, Channel::black), 0xDD);
    }

    for (uint8_t* ptr(buffer.begin()); ptr != buffer.end(); ++ptr)
        EXPECT_EQ(*ptr, 0xDD);

    EXPECT_EQ(*buffer.rbegin(), buffer.readAt(15, Channel::black));
}

TEST(test_iterator, normal_iterators_with_solid_color)
{
    PixelBuffer buffer(16, 4);
    for (uint8_t* ptr(buffer.begin()); ptr != buffer.end(); ptr += 4)
    {
        ptr[Channel::cyan] = 0xAB;
        ptr[Channel::magenta] = 0xCD;
        ptr[Channel::yellow] = 0xEF;
        ptr[Channel::black] = 0xFE;
    }

    for (size_t i = 0; i < 16; ++i)
    {
        EXPECT_EQ(buffer.readAt(i, Channel::cyan), 0xAB);
        EXPECT_EQ(buffer.readAt(i, Channel::magenta), 0xCD);
        EXPECT_EQ(buffer.readAt(i, Channel::yellow), 0xEF);
        EXPECT_EQ(buffer.readAt(i, Channel::black), 0xFE);
    }

    for (uint8_t* ptr(buffer.begin()); ptr != buffer.end(); ptr += 4)
    {
        EXPECT_EQ(ptr[Channel::cyan], 0xAB);
        EXPECT_EQ(ptr[Channel::magenta], 0xCD);
        EXPECT_EQ(ptr[Channel::yellow], 0xEF);
        EXPECT_EQ(ptr[Channel::black], 0xFE);
    }

    EXPECT_EQ(*(buffer.rbegin() + Channel::cyan), 0xAB);
    EXPECT_EQ(*(buffer.rbegin() + Channel::magenta), 0xCD);
    EXPECT_EQ(*(buffer.rbegin() + Channel::yellow), 0xEF);
    EXPECT_EQ(*(buffer.rbegin() + Channel::black), 0xFE);
}


TEST(test_iterator, reverse_iterators_with_solid_white_color)
{
    PixelBuffer buffer(16, 4);
    for (uint8_t* ptr(buffer.end() - 1); ptr != buffer.begin() - 1; --ptr)
        *ptr = 0xDD;

    for (size_t i = 0; i < 16; ++i)
    {
        EXPECT_EQ(buffer.readAt(i, Channel::cyan), 0xDD);
        EXPECT_EQ(buffer.readAt(i, Channel::magenta), 0xDD);
        EXPECT_EQ(buffer.readAt(i, Channel::yellow), 0xDD);
        EXPECT_EQ(buffer.readAt(i, Channel::black), 0xDD);
    }
}

TEST(test_iterator, reverse_iterators_with_solid_color)
{
    PixelBuffer buffer(16, 4);
    for (uint8_t* ptr(buffer.rbegin()); ptr > buffer.rend(); ptr -= 4)
    {
        ptr[Channel::cyan] = 0xAB;
        ptr[Channel::magenta] = 0xCD;
        ptr[Channel::yellow] = 0xEF;
        ptr[Channel::black] = 0xFE;
    }

    for (size_t i = 0; i < 16; ++i)
    {
        EXPECT_EQ(buffer.readAt(i, Channel::cyan), 0xAB) << i;
        EXPECT_EQ(buffer.readAt(i, Channel::magenta), 0xCD) << i;
        EXPECT_EQ(buffer.readAt(i, Channel::yellow), 0xEF) << i;
        EXPECT_EQ(buffer.readAt(i, Channel::black), 0xFE) << i;
    }
}
