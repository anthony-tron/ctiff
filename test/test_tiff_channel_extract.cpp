#include <gtest/gtest.h>
#include "Tiff.h"

#define PATH_TO_IMAGE "/var/public_img/libtiff/pc260001.tif"

using ctiff::Channel;
using ctiff::PixelBuffer;
using ctiff::Tiff;

TEST(test_tiff_channel_extract, pixel_buffer_test)
{
    PixelBuffer buffer(16, 4);
    for (uint8* ptr(buffer.begin()); ptr != buffer.end(); ptr += 4)
    {
        ptr[Channel::cyan] = 0xAB;
        ptr[Channel::magenta] = 0xCD;
        ptr[Channel::yellow] = 0xEF;
        ptr[Channel::black] = 0xFE;
    }

    PixelBuffer cyan(buffer.extract(Channel::cyan));
    PixelBuffer magenta(buffer.extract(Channel::magenta));
    PixelBuffer yellow(buffer.extract(Channel::yellow));
    PixelBuffer black(buffer.extract(Channel::black));

    for (uint8* ptr(cyan.begin()); ptr != cyan.end(); ++ptr)
        EXPECT_EQ(*ptr, 0xAB);

    for (uint8* ptr(magenta.begin()); ptr != magenta.end(); ++ptr)
        EXPECT_EQ(*ptr, 0xCD);

    for (uint8* ptr(yellow.begin()); ptr != yellow.end(); ++ptr)
        EXPECT_EQ(*ptr, 0xEF);

    for (uint8* ptr(black.begin()); ptr != black.end(); ++ptr)
        EXPECT_EQ(*ptr, 0xFE);
}


TEST(test_tiff_channel_extract, extract_and_save_from_pixel_buffer)
{
    const uint32_t width = 32;
    const uint32_t height = 32;
    const uint16_t samples = 4;

    PixelBuffer buffer(width * height, samples);
    for (uint8* ptr(buffer.begin()); ptr != buffer.end(); ptr += 4)
    {
        ptr[Channel::cyan] = 0xAB;
        ptr[Channel::magenta] = 0xCD;
        ptr[Channel::yellow] = 0xEF;
        ptr[Channel::black] = 0xFE;
    }

    typedef std::map<Channel, PixelBuffer> layer_map_t;
    layer_map_t layers{};
    layers.insert(layer_map_t::value_type(Channel::cyan, buffer.extract(Channel::cyan)));
    layers.insert(layer_map_t::value_type(Channel::magenta, buffer.extract(Channel::magenta)));
    layers.insert(layer_map_t::value_type(Channel::yellow, buffer.extract(Channel::yellow)));
    layers.insert(layer_map_t::value_type(Channel::black, buffer.extract(Channel::black)));

    for (layer_map_t::iterator layer(layers.begin()); layer != layers.end(); ++layer)
    {
        Tiff tiff("/var/public_img/test_tiff_channel_extract-" + std::to_string(layer->first) + ".tiff", Tiff::AccessMode::write);
        tiff.withDimensions(width, height)
            .withBitsPerSample(8)
            .withSamplesPerPixel(1)
            .withColorSpace(PHOTOMETRIC_MINISWHITE)
            .withContiguousFormat();

        for (size_t i = 0; i < height; ++i)
            tiff.writeLine(layer->second + i * width, i);
    }
}


TEST(test_tiff_channel_extract, extract_and_save_from_file)
{
    const std::string srcPath("/var/public_img/t6.tiff");
    Tiff src(srcPath, Tiff::AccessMode::read);

    const uint32_t width(src.width());
    const uint32_t height(src.height());
    const uint16_t channelCount(src.field<uint16_t>(TIFFTAG_SAMPLESPERPIXEL));

    std::vector<Tiff> out;
    for (uint16_t i = 0; i < channelCount; ++i)
    {
        out.push_back(Tiff(srcPath + std::to_string(i) + ".tiff", Tiff::AccessMode::write));
        out[i].withDimensions(width, height)
            .withBw()
            .withContiguousFormat();
    }

    for (uint16_t i = 0; i < height; ++i)
    {
        PixelBuffer line(src.scanLine(i));

        for (uint16_t j = 0; j < channelCount; ++j)
            out[j].writeLine(line.extract(static_cast<Channel>(j)), i);
    }
}

