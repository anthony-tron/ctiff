#include <gtest/gtest.h>
#include "Tiff.h"

using ctiff::Channel;
using ctiff::PixelBuffer;
using ctiff::Tiff;

TEST(test_tiff_read, simple_fields) {
    Tiff tiff("/var/public_img/aplat-rouge-004.tiff", Tiff::AccessMode::read);
    ASSERT_NO_THROW({
        ASSERT_TRUE(tiff.width() > 0);
        ASSERT_TRUE(tiff.height() > 0);
    });
}

TEST(test_tiff_read, rgba) {
    Tiff tiff("/var/public_img/aplat-rouge-004.tiff", Tiff::AccessMode::read);

    PixelBuffer buffer(tiff.toRGBA());

    EXPECT_EQ(buffer.readAt(0, Channel::red), 0xDD);
    EXPECT_EQ(buffer.readAt(0, Channel::green), 0x00);
    EXPECT_EQ(buffer.readAt(0, Channel::blue), 0x00);
    EXPECT_EQ(buffer.readAt(0, Channel::alpha), 0xFF);
}

TEST(test_tiff_read, scanFirstLine) {
    Tiff tiff("/var/public_img/aplat-rouge-004.tiff", Tiff::AccessMode::read);
    PixelBuffer buffer(tiff.scanLine(0));

    EXPECT_EQ(buffer.readAt(0, Channel::cyan), 0x00);
    EXPECT_EQ(buffer.readAt(0, Channel::magenta), 0xFF);
    EXPECT_EQ(buffer.readAt(0, Channel::yellow), 0xFF);
    EXPECT_EQ(buffer.readAt(0, Channel::black), 0x22);
}

TEST(test_tiff_read, scanEveryLineUsingLibtiff)
{
    TIFF* tif = TIFFOpen("/var/public_img/aplat-rouge-004.tiff", "r");
    if (tif)
    {
        uint32 imagelength;
        uint32 imagewidth;

        TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imagelength);
        TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imagewidth);
        const tmsize_t scanLineSize = TIFFScanlineSize(tif);
        //buf = _TIFFmalloc(scanLineSize);
        uint8* buf = new uint8[scanLineSize];

        for (uint32 row = 0; row < imagelength; row++)
        {
            TIFFReadScanline(tif, buf, row);

            for (uint32 i = 0; i < imagewidth / 4; ++i)
            {
                uint32 offset(i*4);
                EXPECT_EQ(buf[offset + 0], 0x00);
                EXPECT_EQ(buf[offset + 1], 0xFF);
                EXPECT_EQ(buf[offset + 2], 0xFF);
                EXPECT_EQ(buf[offset + 3], 0x22);
            }
        }
        delete[] buf;
        TIFFClose(tif);
    }
}

TEST(test_tiff_read, scanEveryLineProgramatically) {
    Tiff tiff("/var/public_img/aplat-rouge-004.tiff", Tiff::AccessMode::read);

    for (uint32 i = 0; i < tiff.height(); ++i)
    {
        PixelBuffer buffer(tiff.scanLine(i));

        EXPECT_EQ(buffer.readAt(0, Channel::cyan), 0x00);
        EXPECT_EQ(buffer.readAt(0, Channel::magenta), 0xFF);
        EXPECT_EQ(buffer.readAt(0, Channel::yellow), 0xFF);
        EXPECT_EQ(buffer.readAt(0, Channel::black), 0x22);
    }
}

TEST(test_tiff_read, scanEveryLineUsingApi) {

    Tiff tiff("/home/anthony/Pictures/tiff/ctiff.tiff", Tiff::AccessMode::read);
    PixelBuffer buffer(tiff.scanEveryLine());

    for (size_t i = 0; i < tiff.width() * tiff.height(); i += 4)
    {
        EXPECT_EQ(buffer.readAt(i, Channel::cyan), 0x00);
        EXPECT_EQ(buffer.readAt(i, Channel::magenta), 0xFF);
        EXPECT_EQ(buffer.readAt(i, Channel::yellow), 0xFF);
        EXPECT_EQ(buffer.readAt(i, Channel::black), 0x22);
    }
}

TEST(test_tiff_read, handle_failed_to_open_tiffs)
{
    EXPECT_ANY_THROW({
        Tiff tiff("/dz/dzq:dzq:d//DzqdqQ//dzqdzq/", Tiff::AccessMode::read);
    });
}
