#include <gtest/gtest.h>
#include "Tiff.h"

using ctiff::Channel;
using ctiff::PixelBuffer;
using ctiff::Tiff;

TEST(test_tiff_write, test_fields) {
    ASSERT_NO_THROW({
        const uint32 width = 800;
        const uint32 height = 600;

        Tiff tiff("/home/anthony/Pictures/tiff/anyfile.tiff", Tiff::AccessMode::write);

        tiff.withContiguousFormat()
            .withDimensions(width, height)
            .withCmyk()
            .withField<uint16>(TIFFTAG_COMPRESSION, COMPRESSION_LZW)
            .withField<const char*>(TIFFTAG_ARTIST, "Nobody");

        ASSERT_EQ(tiff.field<uint16>(TIFFTAG_PLANARCONFIG), PLANARCONFIG_CONTIG);
        ASSERT_EQ(tiff.field<uint32>(TIFFTAG_IMAGEWIDTH), width);
        ASSERT_EQ(tiff.field<uint32>(TIFFTAG_IMAGELENGTH), height);
        ASSERT_EQ(tiff.field<uint16>(TIFFTAG_PHOTOMETRIC), PHOTOMETRIC_SEPARATED);
        ASSERT_EQ(tiff.field<uint16>(TIFFTAG_SAMPLESPERPIXEL), 4);
        ASSERT_EQ(tiff.field<uint16>(TIFFTAG_BITSPERSAMPLE), 8);
        ASSERT_EQ(tiff.field<uint16>(TIFFTAG_COMPRESSION), COMPRESSION_LZW);
        ASSERT_STREQ(tiff.field<const char*>(TIFFTAG_ARTIST), "Nobody");
    });
}


TEST(test_tiff_write, test_fields_that_should_throw) {

    Tiff tiff("/home/anthony/Pictures/tiff/anyfile.tiff", Tiff::AccessMode::write);

    EXPECT_ANY_THROW({
        tiff.withDimensions(0, 0);
    }) << "Width or Height 0 should be illegal";

    EXPECT_ANY_THROW({
        tiff.withColorSpace(0xFFFF);
    }) << "Photometric should match one of the expected values";

    EXPECT_ANY_THROW({
        tiff.withBitsPerSample(0);
    }) << "Bits per sample should not be zero";

    EXPECT_ANY_THROW({
        tiff.withSamplesPerPixel(0);
    }) << "Samples per pixel should not be zero";
}


TEST(test_tiff_write, test_death) {

    Tiff tiff("/home/anthony/Pictures/tiff/anyfile.tiff", Tiff::AccessMode::write);

    EXPECT_DEATH({
        tiff.withField<uint32>(TIFFTAG_ARTIST, 0xABCDEF);
        // this tag should be a const char*
    }, "");
}


TEST(test_tiff_write, test_image_creation_with_random_background_using_scanline) {
    ASSERT_NO_THROW({
        const uint32 width = 16;
        const uint32 height = 16;

        Tiff tiff("/home/anthony/Pictures/tiff/ctiff.tiff", Tiff::AccessMode::write);

        tiff.withField<uint16>(TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG)
            .withDimensions(width, height)
            .withCmyk()
            .withField<uint16>(TIFFTAG_COMPRESSION, COMPRESSION_LZW)
            .withField<const char*>(TIFFTAG_ARTIST, "Nobody");

        PixelBuffer randomLine(width, 4);
        for (uint32 i = 0; i < width; ++i)
            randomLine.at(i)
                .write(Channel::cyan, 0x00)
                .write(Channel::magenta, 0xFF)
                .write(Channel::yellow, 0xFF)
                .write(Channel::black, 0x22);

        for (uint32 i = 0; i < height; ++i)
        {
            ASSERT_NO_THROW({
                tiff.writeLine(randomLine, i);
            });
        }
    });
}
